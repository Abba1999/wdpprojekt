<?php

require 'Routing.php';


$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);

Routing::get('','DefaultController');
Routing::get('movies','MovieController');
Routing::get('rates','RateController');
Routing::post('login','SecurityController');
Routing::post('register','SecurityController');
Routing::post('logout','SecurityController');
Routing::post('addMovie','MovieController');
Routing::post('addRate','RateController');
Routing::get('category','DefaultController');
Routing::get('profile','DefaultController');
Routing::post('search', 'RateController');
Routing::post('search2', 'MovieController');
Routing::get('like', 'RateController');
Routing::get('dislike', 'RateController');
Routing::post('MoviesCategories', 'CategoryController');
Routing::get('profile','ProfileController');
Routing::get('chooses','ChooseController');
Routing::get('watch','ChooseController');
Routing::get('chooseMovie','ChooseController');

Routing::run($path);

?>