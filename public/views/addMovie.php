<?php require 'sessionsecutiry.php';
if (!$_SESSION['role']==2)
{
    header("Location: ./movies");
    die();
}
?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/rates.css">
    <script src="https://kit.fontawesome.com/4d4ef762b0.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/movieSearch.js" defer></script>
    <script type="text/javascript" src="./public/js/menu.js" defer></script>

    <title>MAIN PAGE</title>
</head>
<body>
<div class="base-container">
    <?php require 'menu.php'; ?>
    <main>
        <?php require 'search&more.php'; ?>
        <section class="movie-add">
            <form class ="form-rate" action="addMovie" method="POST" ENCTYPE="multipart/form-data">

                <section class="write">
                    <?php
                    if(isset($messages)){
                        foreach($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                    <div id="write-1">

                        <input name = "title" type="text" placeholder="Title">

                        <div class="rate-icon">
                            <i class="fas fa-pen"></i>
                        </div>
                    </div>

                    <div id="write-2">

                        <input name = "viewerAge" type="number" min="1" max="18" placeholder="ViewerAge">

                        <div class="rate-icon">
                            <i class="fas fa-user-clock"></i>
                        </div>
                    </div>
                    <div id="write-3">

                        <input name = "year" type="number" min="1901"  placeholder="Year">

                        <div class="rate-icon">
                            <i class="fas fa-clock"></i>
                        </div>
                    </div>
                    <div id="write-4">

                        <input name = "continent" type="text" placeholder="Continent">

                        <div class="rate-icon">
                            <i class="fas fa-globe-europe"></i>
                        </div>
                    </div>


                    <div id="input-file">
                        <input type="file" name="file"></input>
                        <div class="rate-icon">
                            <i class="fas fa-star"></i>
                        </div>
                    </div>

                    <div id="submit">
                        <button type="submit">
                            <i class="fas fa-check"></i>
                            Submit
                        </button>
                    </div>

                </section>
                <section class="article-rate">
                    <div id="Tabula-Rasa">
                        <p><b>Tabula Rasa</b></p>
                    </div>



                    <div id="writing">
                        <textarea name="description" placeholder="description" id="rate-text"></textarea>
                    </div>
                </section>
            </form>

        </section>
    </main>
</div>
</body>