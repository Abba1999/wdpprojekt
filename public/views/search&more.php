<header>
    <div class="search-bar">
        <input placeholder="search">
    </div>

    <div class="buttons-top-all">
    <a id="menu-top" class="icon" onclick="menu()">
        <i class="fa fa-bars"></i>
    </a>

    <form id="buttons-top" class="buttons-top" action="logout" method="POST">
        <a href="profile" class="profil">
            <i class="fa fa-user"></i>
        </a>
        <button type="submit" class="logout" >
            <i class="fa fa-sign-out"></i>
        </button>

    </form>
    </div>
</header>