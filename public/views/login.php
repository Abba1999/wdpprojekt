<?php session_start(); ?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <title>LOGIN PAGE</title>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="login-container">
            <form class="login" action="login" method="POST">
                <div class="message">
                    <?php if(isset($messages)){
                        foreach ($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <input name="email" type="text" placeholder="email@email.com" value="<?php if(isset($_COOKIE["email"]))?>">
                <input name="password" type="password" placeholder="password" value="<?php if(isset($_COOKIE["password"]))?>">
                <div class="check-button">
                    <input class ="remember" type="checkbox" name="remember"/>
                    <label>Remember</label>
                </div>
                <div class="buttons">
                    <button type="submit">LOGIN</button>
                    <button class="button-menu" onclick="location.href='register'" type="button">-</button>
                </div>
            </form>
        </div>
    </div>
</body>