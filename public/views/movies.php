<?php require 'sessionsecutiry.php';?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/movies.css">
    <script src="https://kit.fontawesome.com/4d4ef762b0.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/movieSearch.js" defer></script>
    <script type="text/javascript" src="./public/js/menu.js" defer></script>

    <title>MAIN PAGE</title>
</head>
<body>
    <div class="base-container">
        <?php require 'menu.php'; ?>
        <main>
            <?php require 'search&more.php'; ?>
            <section class="movies">
                <?php foreach ($movies as $movie): ?>

                    <div id="<?= $movie->getID()?>" action="getMovie">
                        <img src="public/img/uploads/<?= $movie->getImage() ?>">
                        <div>
                            <h2><?= $movie->getTitle(); ?></h2>
                            <p class = description><?= $movie->getDescription();?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
                
            </section>
        </main>
    </div>
</body>

<template id="movie-template">
    <div id="">
        <img src="">
        <div>
            <h2>title</h2>
            <p class = description>description</p>
        </div>
    </div>
</template>