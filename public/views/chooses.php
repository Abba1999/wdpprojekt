<?php require 'sessionsecutiry.php';?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/choose.css">
    <script type="text/javascript" src="./public/js/movieSearch.js" defer></script>
    <script type="text/javascript" src="./public/js/menu.js" defer></script>
    <script src="https://kit.fontawesome.com/4d4ef762b0.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="./node_modules/@glidejs/glide/dist/css/glide.core.min.css">
    <script type="text/javascript" src="./public/js/glider.js" defer></script>
    <script src="node_modules/@glidejs/glide/dist/glide.min.js"></script>



    <title>MAIN PAGE</title>
</head>
<body>
<div class="base-container">
    <?php require 'menu.php'; ?>
    <main>
        <?php require 'search&more.php'; ?>
        <section class="choose">
            <form class="elements" method="POST" action="watch">
                <h3>Genre</h3>
                <select class="dropdown-category" id="category" name="cat-dd">
                    <option name="category-option" value="Akcja">Action</option>
                    <option name="category-option" value="Dramat">Drama</option>
                    <option name="category-option" value="Fantasy">Fantasy</option>
                    <option name="category-option" value="Komedia">Comedy</option>
                    <option name="category-option" value="Horror">Horror</option>
                    <option name="category-option" value="Sci-Fi">Sci-Fi</option>
                    <option name="category-option" value="Psychologiczny">Psychological</option>
                    <option name="category-option" value="Przygodowy">Adventure</option>
                    <option name="category-option" value="Romans">Romance</option>
                    <option name="category-option" value="Sportowy">Sport</option>
                    <option name="category-option" value="Historyczny">History</option>
                    <option name="category-option" value="Obyczajowy">Family</option>
                </select>
                <h3>Friend</h3>
                <select class="dropdown-friend" id="friend">
                    <option name="friend-option" value="id">Nick</option>
                </select>
                <div id="submit">
                    <button type="submit" name="submit">
                        <i class="fas fa-check"></i>
                        Submit
                    </button>
                </div>
            </form>

            <div class="glide-area">
            <div class="glide">

                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
                        <?php foreach ($chooses as $movie): ?>
                            <li class="glide__slide">
                                <div id="<?= $movie->getID()?>" class="movie-slide">
                                    <img src="public/img/uploads/<?= $movie->getImage() ?>">
                                    <div>
                                        <h2><?= $movie->getTitle() ?></h2>
                                        <p class = description><?= $movie->getDescription() ?></p>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="glide__arrows" data-glide-el="controls">
                    <button class="glide__arrow glide__arrow--left" data-glide-dir="<">prev</button>
                    <button class="glide__arrow glide__arrow--right" data-glide-dir=">">next</button>
                </div>

                <div class="glide__bullets" data-glide-el="controls[nav]">
                    <button class="glide__bullet" data-glide-dir="<<"></button>
                    <button class="glide__bullet" data-glide-dir=">>"></button>
                </div>

            </div>
            </div>
        </section>


</body>



<template id="choose-template">
    <<li class="glide__slide">
        <div id="">
            <img src="">
            <div>
                <h2>title</h2>
                <p class = description>description</p>
            </div>
        </div>
    </li>
</template>