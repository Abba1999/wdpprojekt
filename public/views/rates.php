<?php require 'sessionsecutiry.php'; ?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/movies.css">
    <script src="https://kit.fontawesome.com/4d4ef762b0.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/search.js" defer></script>
    <script type="text/javascript" src="./public/js/statistics.js" defer></script>
    <script type="text/javascript" src="./public/js/menu.js" defer></script>

    <title>MAIN PAGE</title>
</head>
<body>
<div class="base-container">
    <?php require 'menu.php'; ?>
    <main>
        <?php require 'search&more.php'; ?>

            <section class="rates">
                <?php foreach ($rates as $rate): ?>

                <div id="<?= $rate->getID()?>" action="getRate"  >
                    <img src="public/img/uploads/<?= $rate->getImage() ?>">
                    <div>
                        <h2><?= $rate->getTitle();?></h2>
                        <p class = description><?= $rate->getDescription();?></p>
                        <div class="social-section">
                            <i class="fas fa-heart"><?= $rate->getLikes()?></i>
                            <i class="fas fa-star"><?= $rate->getStars()?></i>
                            <i class="fas fa-minus-square"><?= $rate->getDislikes()?></i>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>

            </section>
        </main>
    </div>
</body>

<template id="rate-template">
    <div id="">
        <img src="">
        <div>
            <h2>title</h2>
            <p class = description>description</p>
            <div class="social-section">
                <i class="fas fa-heart">0</i>
                <i class="fas fa-star">0</i>
                <i class="fas fa-minus-square">0</i>
            </div>
        </div>
    </div>
</template>