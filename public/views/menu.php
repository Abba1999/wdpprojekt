<nav>
    <img src="public/img/logo.svg">
    <ul class="menu-left-ul">
        <li>
            <i class="fas fa-film"></i>
            <a href="movies" class="button">Movies</a>
        </li>
        <li>
            <i class="fas fa-star"></i>
            <a href="rates" class="button">Reviews</a>
        </li>
        <li>
            <i class="fas fa-search"></i>
            <a name="left_menu" href="category" class="button">Category</a>
        </li>
        <li>
            <i class="fas fa-hand-point-up"></i>
            <a href="chooses" class="button">Watch</a>
        </li>
        <li>
            <i class="fas fa-pen-square"></i>
            <a href="addRate" class="button">AddReview</a>
        </li>
    </ul>
</nav>