<?php require 'sessionsecutiry.php'; ?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/profile.css">
    <script src="https://kit.fontawesome.com/4d4ef762b0.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/movieSearch.js" defer></script>
    <script type="text/javascript" src="./public/js/menu.js" defer></script>

    <title>MAIN PAGE</title>
</head>
<body>
<div class="base-container">
    <?php require 'menu.php'; ?>
    <main>
        <?php require 'search&more.php'; ?>
        <section class="profile">
            <div class="person">

                <img src="public/img/icons/<?= $profile->getFriend0(); ?>">

                <h1><?= $profile->getNick(); ?></h1>

                <h3>Favourite categories</h3>
                <div class="cate">
                    <p class = "movie"><?= $profile->getFavC1(); ?></p>
                    <p class = "cate"><?= $profile->getFavC2(); ?></p>
                    <p class = "cate"><?= $profile->getFavC3(); ?></p>
                </div>
            </div>
            <div class="friends">
                <h2 class="friends-title">
                    Friends
                    <i class="fas fa-user-friends"></i>
                </h2>

                <img src="public/img/icons/<?= $profile->getFriend1(); ?>">
                <img src="public/img/icons/<?= $profile->getFriend2(); ?>">

            </div>
            <div class="reviews">
                <h2>
                    Favourite reviews
                    <i class="fas fa-star"></i>
                </h2>
                <p class = "fav"><?= $profile->getFavR1(); ?></p>
                <p class = "fav"><?= $profile->getFavR2(); ?></p>
                <p class = "fav"><?= $profile->getFavR3(); ?></p>
            </div>
            <div class="movies">
                <h2>
                    Favourite movies
                    <i class="fas fa-film"></i>
                </h2>
                <p class = "fav"><?= $profile->getFavM1(); ?></p>
                <p class = "fav"><?= $profile->getFavM2(); ?></p>
                <p class = "fav"><?= $profile->getFavM3(); ?></p>

            </div>

        </section>
    </main>
</div>
</body>