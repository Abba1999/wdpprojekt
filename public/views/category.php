<?php require 'sessionsecutiry.php'; ?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/category.css">
    <script src="https://kit.fontawesome.com/4d4ef762b0.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/movieSearch.js" defer></script>
    <script type="text/javascript" src="./public/js/menu.js" defer></script>

    <title>MAIN PAGE</title>
</head>
<body>
<div class="base-container">
    <?php require 'menu.php'; ?>
    <main>
        <?php require 'search&more.php'; ?>
        <section class="category">
            <form class="elements" method="POST" action="MoviesCategories">
                <h3>Viewer Age</h3>
                <div class="cate-body">
                    <div class="radio-button">
                        <input type="radio" name="age" value="1" checked="checked">
                        <label>For everyone</label>
                    </div>
                    <div class="radio-button">
                        <input type="radio" name="age" value="10">
                        <label>-13</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="age" value="13">
                        <label>13-16</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="age" value="16">
                        <label>16-18</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="age" value="18">
                        <label>+18</label>
                    </div>
                </div>


                <h3>Genre</h3>
                <div class="cate-body">
                    <div class="check-button">
                        <input type="checkbox" name="check[]" value="Akcja">
                        <label>Action</label>
                    </div>
                    <div class="check-button">

                        <input type="checkbox" name="check[]" value="Dramat">
                        <label>Drama</label>
                    </div>
                    <div class="check-button">

                        <input type="checkbox" name="check[]" value="Fantasy">
                        <label>Fantasy</label>
                    </div>
                    <div class="check-button">

                        <input type="checkbox" name="check[]" value="Komedia">
                        <label>Comedy</label>
                    </div>
                    <div class="check-button">

                        <input type="checkbox" name="check[]" value="Horror">
                        <label>Horror</label>
                    </div>
                    <div class="check-button">

                        <input type="checkbox" name="check[]" value="Sci-Fi">
                        <label>Sci-Fi</label>
                    </div>
                    <div class="check-button">

                        <input type="checkbox" name="check[]" value="Psychologiczny">
                        <label>Psychological</label>
                    </div>
                    <div class="check-button">

                        <input type="checkbox" name="check[]" value="Przygodowy">
                        <label>Adventure</label>
                    </div>
                    <div class="check-button">

                        <input type="checkbox" name="check[]" value="Romans">
                        <label>Romance</label>
                    </div>
                    <div class="check-button">

                        <input type="checkbox" name="check[]" value="Sportowy">
                        <label>Sport</label>
                    </div>
                    <div class="check-button">

                        <input type="checkbox" name="check[]" value="Historyczny">
                        <label>History</label>
                    </div>
                    <div class="check-button">

                        <input type="checkbox" name="check[]" value="Obyczajowy">
                        <label>Family</label>
                    </div>
                </div>

                <h3>Year of Movie</h3>
                <div class="cate-body">
                    <div class="radio-button">

                        <input type="radio" name="year" value="1901">
                        <label>1900-1950</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="year" value="1951">
                        <label>1951-1980</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="year" value="1981">
                        <label>1981-2000</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="year" value="2001">
                        <label>2001-2010</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="year" value="2011">
                        <label>2011-2020</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="year" value="2021">
                        <label>2021+</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="year" value="3000" checked="checked">
                        <label>All years</label>
                    </div>

                </div>

                <h3>Where Made</h3>
                <div class="cate-body">
                    <div class="radio-button">

                        <input type="radio" name="continent" value="North America">
                        <label>North America</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="continent" value="South America">
                        <label>South America</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="continent" value="Europe">
                        <label>Europe</label>
                    </div>
                    <div class="radio-button">
                        <input type="radio" name="continent" value="Africa">
                        <label>Africa</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="continent" value="Asia">
                        <label>Asia</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="continent" value="Australia">
                        <label>Australia</label>
                    </div>
                    <div class="radio-button">

                        <input type="radio" name="continent" value="World" checked="checked">
                        <label>World</label>
                    </div>

                </div>

                <div id="submit">
                    <button type="submit" name="submit" >
                        <i class="fas fa-check"></i>
                        Submit
                    </button>
                </div>
            </form>
        </section>
    </main>
</div>
</body>