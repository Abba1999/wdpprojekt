const search = document.querySelector('input[placeholder="search"]');
const movieContainer = document.querySelector(".movies");

search.addEventListener("keyup", function (event) {
    console.log("abba");

    if (event.key === "Enter") {
        event.preventDefault();

        const data = {search: this.value};


        fetch("/search2", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response) {
            //alert("aa");
            return response.json();
        }).then(function (movies) {
            movieContainer.innerHTML = "";
            loadMovies(movies)
        }).catch(error => {
            alert("Bb");
        });
    }
});

function loadMovies(movies) {
    movies.forEach(movie => {
        console.log(movie);
        createMovie(movie);
    });
}

function createMovie(movie) {
    const template = document.querySelector("#movie-template");

    const clone = template.content.cloneNode(true);
    const div = clone.querySelector("div");
    div.id = movie.id;
    const image = clone.querySelector("img");
    image.src = `/public/img/uploads/${movie.image}`;
    const title = clone.querySelector("h2");
    title.innerHTML = movie.title;
    const description = clone.querySelector("p");
    description.innerHTML = movie.description;

    movieContainer.appendChild(clone);
}
