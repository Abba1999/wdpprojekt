const search = document.querySelector('input[placeholder="search"]');
const rateContainer = document.querySelector(".rates");

search.addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();

        const data = {search: this.value};

        fetch("/search", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response) {
            return response.json();
        }).then(function (rates) {
            rateContainer.innerHTML = "";
            loadRates(rates)
        }).catch(error => {
            alert("Aa");
        });
    }
});

function loadRates(rates) {
    rates.forEach(rate => {
        console.log(rate);
        createRate(rate);
    });
}

function createRate(rate) {
    const template = document.querySelector("#rate-template");

    const clone = template.content.cloneNode(true);
    const div = clone.querySelector("div");
    div.id = rate.id;
    const image = clone.querySelector("img");
    image.src = `/public/img/uploads/${rate.image}`;
    const title = clone.querySelector("h2");
    title.innerHTML = rate.title;
    const description = clone.querySelector("p");
    description.innerHTML = rate.description;
    const likes = clone.querySelector(".fa-heart");
    likes.innerText = rate.likes;
    const stars = clone.querySelector(".fa-star");
    stars.innerText = rate.stars;
    const dislikes = clone.querySelector(".fa-minus-square");
    dislikes.innerText = rate.dislikes;

    rateContainer.appendChild(clone);
}
