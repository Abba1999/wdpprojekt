let x = 3;
let gap = 100;
let media = window.matchMedia("(max-width: 1000px)");
if(media.matches){
    x=1;
    gap = 200;
}

const config = {
    type: 'carousel',
    startAt: 0,
    perView: x,
    focusAt: 'center',
    gap: gap,
    keyboard: true,
    bound: true,
}
new Glide('.glide',config).mount();

const chooseButtons = document.querySelectorAll(".movie-slide");

function chooseMovie() {
    const movies = this;
    const id = movies.getAttribute("id");

    fetch(`/chooseMovie/${id}`)
        .then(function(){
        })
}

chooseButtons.forEach(button => button.addEventListener("click", chooseMovie));