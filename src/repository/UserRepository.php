<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';

class UserRepository extends Repository{

    public function getUser(string $email): ?User{

        $stmt = $this->database->connect()->prepare('
          SELECT * FROM users u LEFT JOIN user_details ud ON u.id_user_details = ud.id
          WHERE email = :email
        ');

        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false){
            return null;    //wyrzucic wyjatek
        }

        return new User(
            $user['email'],
            $user['password'],
            $user['name'],
            $user['surname'],
            $user['id'],
            $user['salt'],
            $user['role_a']
        );


    }

    public function addUser(User $user)
    {




        $stmt = $this->database->connect()->prepare('
            INSERT INTO user_details (name, surname, nick)
            VALUES (?, ?, ?)
        ');

        $stmt->execute([
            $user->getName(),
            $user->getSurname(),
            $user->getNick()
        ]);

        $stmt = $this->database->connect()->prepare('
            INSERT INTO users (id,email, password, id_user_details,salt)
            VALUES (?, ?, ?, ?,?)
        ');

        $stmt->execute([
            $this->getUserDetailsId($user),
            $user->getEmail(),
            $user->getPassword(),
            $this->getUserDetailsId($user),
            $user->getSalt()
        ]);

        $stmt = $this->database->connect()->prepare('
            INSERT INTO social (id,id_users)
            VALUES (?,?)
        ');
        $stmt->execute([
            $this->getUserDetailsId($user),
            $this->getUserDetailsId($user),
        ]);

        $stmt = $this->database->connect()->prepare('
            INSERT INTO relations (friend_1,friend_2)
            VALUES (?,2), (?,1)
        ');
        $stmt->execute([
            $this->getUserDetailsId($user),
            $this->getUserDetailsId($user),
        ]);





    }

    public function getUserDetailsId(User $user): int
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.user_details WHERE name = :name AND surname = :surname AND nick = :nick
        ');
        $stmt->bindParam(':name', $user->getName(), PDO::PARAM_STR);
        $stmt->bindParam(':surname', $user->getSurname(), PDO::PARAM_STR);
        $stmt->bindParam(':nick', $user->getNick(), PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['id'];
    }

    public function getUserID(User $user): int{
        $stmt = $this->database->connect()->prepare('
            SELECT id FROM public.users WHERE email = :email AND salt = :salt
        ');
        $stmt->bindParam(':email', $user->getEmail(), PDO::PARAM_STR);
        $stmt->bindParam(':salt', $user->getSalt(), PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['id'];
    }

    public function enableUpdate(int $id_users, int $el){

        $stmt = $this->database->connect()->prepare('
        UPDATE users SET enabled = :el WHERE id = :id
        ');
        $stmt->bindParam(':el', $el , PDO::PARAM_INT);
        $stmt->bindParam(':id', $id_users , PDO::PARAM_INT);
        $stmt->execute();

    }


}