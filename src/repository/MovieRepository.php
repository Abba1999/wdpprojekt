<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Movie.php';

class MovieRepository extends Repository{

    public function getMovie(string $id): ?Movie{

        $stmt = $this->database->connect()->prepare('
          SELECT * FROM public.movies WHERE id = :id
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $movie = $stmt->fetch(PDO::FETCH_ASSOC);

        if($movie == false){
            return null;    //wyrzucic wyjatek
        }

        return new Movie(
            $movie['title'],
            $movie['viewer_age'],
            $movie['create_year'],
            $movie['continent'],
            $movie['image'],
            $movie['description'],
            $movie['id']
        );
    }

    public function getMovies(): array{

        $result = [];

        $stmt = $this->database->connect()->prepare('
          SELECT * FROM public.movies
        ');

        $stmt->execute();

        $movies = $stmt->fetchAll(PDO::FETCH_ASSOC);

//

        foreach ($movies as $movie){
            $result[] = new Movie(
                $movie['title'],cd.
                $movie['viewer_age'],
                $movie['create_year'],
                $movie['continent'],
                $movie['image'],
                $movie['description'],
                $movie['id']

            );
        }

        return $result;
    }

    public function addMovie(Movie $movie): void{
        $stmt = $this->database->connect()->prepare('
                INSERT INTO movies (title,viewer_age, create_year, continent,image,description)
                VALUES (?, ?, ?, ?, ?, ?)
            ');


        $stmt->execute([
            $movie->getTitle(),
            $movie->getViewerAge(),
            $movie->getCreateYear(),
            $movie->getContinent(),
            $movie->getImage(),
            $movie->getDescription()
        ]);

    }

    public function getMovieByTitle(string $searchString)
    {
        $searchString = '%' . strtolower($searchString) . '%';

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM movies WHERE LOWER(title) LIKE :searcha OR LOWER(description) LIKE :searcha
        ');
        $stmt->bindParam(':searcha', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}