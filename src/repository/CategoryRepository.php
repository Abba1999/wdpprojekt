<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Movie.php';

class CategoryRepository extends Repository{

    public function getMoviesCategories($ageVal, $geneVal, $yearVal, $continentVal): array{

        $result = [];
        $query = 'SELECT m.title, m.description, m.image, m.id FROM public.movies m, moviesncategories2 mc';

        switch ($ageVal){
            case 10:
                $query = $query.' WHERE viewer_age < 13';
                echo "10";
                break;
            case 13:
                $query = $query.' WHERE viewer_age between 13 and 15';
                echo "13";
                break;
            case 16:
                $query = $query.' WHERE viewer_age between 16 and 17';
                echo "16";
                break;
            case 18:
                $query = $query.' WHERE viewer_age >= 18';
                echo "18";
                break;
            default:
                $query = $query.' WHERE viewer_age > 0';
                break;
        }

        switch($yearVal){
            case 1901:
                $query = $query.' AND create_year between 1900 and 1950';
                break;
            case 1951:
                $query = $query.' AND create_year between 1951 and 1980';
                break;
            case 1981:
                $query = $query.' AND create_year between 1981 and 2000';
                break;
            case 2001:
                $query = $query.' AND create_year between 2001 and 2010';
                break;
            case 2011:
                $query = $query.' AND create_year between 2011 and 2020';
                break;
            case 2021:
                $query = $query.' AND create_year > 2020';
                break;
            default:
                $query = $query.' AND create_year > 0';
                break;
        }

        if($continentVal == "World") {

        }else{
            $query = $query . ' AND continent = :contName';
        }




//        if(!$geneVal == 0){
//            $query = $query." AND name_category IN (";
//            foreach($geneVal as $value){
//                $query = $query."'".$value."',";
//            }
//            $query = substr_replace($query, "", -1);
//            $query = $query.')';
//        }







        $query = $query.' AND mc.title = m.title group by m.title, m.description, m.image,m.id';
        //echo $query;


        if(!$geneVal == 0){
            $query = $query." HAVING";
            foreach($geneVal as $value){
                $query = $query." SUM(CASE WHEN name_category = '".$value."' then 1 else 0 end) > 0 AND";
                echo $value;
            }
            $query = substr_replace($query, "", -1);
            $query = substr_replace($query, "", -1);
            $query = substr_replace($query, "", -1);
        }



        //echo $query;

        $stmt = $this->database->connect()->prepare($query);

        if($continentVal == 'World') {

        } else{
            $stmt->bindParam(':contName', $continentVal, PDO::PARAM_STR);
        }



        $stmt->execute();

        $movies = $stmt->fetchAll(PDO::FETCH_ASSOC);


        foreach ($movies as $movie){
            $result[] = new Movie(
                $movie['title'],
                0,
                0,
                '',
                $movie['image'],
                $movie['description'],
                $movie['id']

            );
        }
//        foreach ($result as $results){
//            echo $results->getTitle();
//        }
        return $result;
    }

}