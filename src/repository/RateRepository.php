<?php

session_start();

require_once 'Repository.php';
require_once __DIR__.'/../models/Rate.php';

class RateRepository extends Repository{

    public function getRate(string $id): ?Rate{

        $stmt = $this->database->connect()->prepare('
          SELECT * FROM public.rates WHERE id = :id
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $rate = $stmt->fetch(PDO::FETCH_ASSOC);

        if($rate == false){
            return null;    //wyrzucic wyjatek
        }

        return new Rate(
            $rate['title'],
            $rate['description'],
            $rate['stars'],
            $rate['image'],
        );
    }

        public function addRate(Rate $rate): void{
            $date = new DateTime();
            $stmt = $this->database->connect()->prepare('
                INSERT INTO rates (title,id_users, description, id_movies, stars, created_at, image)
                VALUES (?, ?, ?, ?, ?, ?, ?)
            ');

            $id_users = $_SESSION['ID'];
            $id_movies = 1;

            $stmt->execute([
                $rate->getTitle(),
                $id_users,
                $rate->getDescription(),
                $id_movies,
                $rate->getStars(),
                $date->format('Y-m-d'),
                $rate->getImage(),
            ]);

        }

    public function getRates(): array{

        $result = [];

        $stmt = $this->database->connect()->prepare('
          SELECT * FROM public.rates
        ');

        $stmt->execute();

        $rates = $stmt->fetchAll(PDO::FETCH_ASSOC);


        foreach ($rates as $rate){
            $result[] = new Rate(
                $rate['title'],
                $rate['description'],
                $rate['stars'],
                $rate['image'],
                $rate['likes'],
                $rate['dislikes'],
                $rate['id']
            );
        }

        return $result;
    }


    public function getRateByTitle(string $searchString)
    {
        $searchString = '%' . strtolower($searchString) . '%';

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM rates WHERE LOWER(title) LIKE :searcha OR LOWER(description) LIKE :searcha
        ');
        $stmt->bindParam(':searcha', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function like(int $id){
        $stmt = $this->database->connect()->prepare('
        UPDATE rates SET likes = likes + 1 WHERE id = :id
        ');
        $stmt->bindParam(':id', $id , PDO::PARAM_INT);
        $stmt->execute();
    }

    public function dislike(int $id){
        $stmt = $this->database->connect()->prepare('
        UPDATE rates SET dislikes = dislikes + 1 WHERE id = :id
        ');
        $stmt->bindParam(':id', $id , PDO::PARAM_INT);
        $stmt->execute();
    }
}