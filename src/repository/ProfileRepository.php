<?php

session_start();

require_once 'Repository.php';
require_once __DIR__.'/../models/Profile.php';
require_once __DIR__.'/../models/Movie.php';

class ProfileRepository extends Repository{

    public function getProfile():?Profile {

        $id = $_SESSION['ID'];
        $stmt = $this->database->connect()->prepare('
          SELECT * FROM public.social WHERE id = :id
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $profile = $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare('
          SELECT m.title as title1, m2.title as title2, m3.title as title3 FROM social s JOIN movies m ON s.fav_m1 = m.id JOIN movies m2 ON  s.fav_m2 = m2.id JOIN movies m3 ON s.fav_m3 = m3.id WHERE s.id = :id
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $movies = $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare('
          SELECT nick FROM public.user_details WHERE id = :id
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $nick = $stmt->fetchColumn();


        $stmt = $this->database->connect()->prepare('
          select icon from user_details where id = :id OR id IN (select distinct friend_1 from all_friendships where friend_2 = :id LIMIT 2);
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $friends = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);


        if($profile == false || $nick == null){
            return null;
        }


        return new Profile(
            $profile['id'],
            $profile['id_users'],
            $movies['title1'],
            $movies['title2'],
            $movies['title3'],
            $profile['fav_r1'],
            $profile['fav_r2'],
            $profile['fav_r3'],
            $profile['fav_c1'],
            $profile['fav_c2'],
            $profile['fav_c3'],
            $nick,
            $friends[0],
            $friends[1],
            $friends[2]
        );
    }





}