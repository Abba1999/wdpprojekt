<?php
session_start();
require_once 'Repository.php';
require_once __DIR__.'/../models/Movie.php';

class ChooseRepository extends Repository{


    public function getMoviesChoose($geneVal): array{

        $result = [];

        $query = 'SELECT m.id, m.title, m.description, m.image FROM public.movies m, moviesncategories2 mc WHERE name_category = :geneVal AND mc.title = m.title group by m.id, m.title, m.description, m.image';
        $stmt = $this->database->connect()->prepare($query);
        $stmt->bindParam(':geneVal', $geneVal, PDO::PARAM_STR);

        $stmt->execute();

        $movies = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($movies as $movie){
            $result[] = new Movie(
                $movie['title'],
                0,
                0,
                '',
                $movie['image'],
                $movie['description'],
                $movie['id']

            );

        }

        return $result;
    }

    function blankMovie(): array{
        $result = [];

        $result[] = new Movie(
          "Blank",
            0,
            0,
            '',
            'blank.jpg',
            'blank',
            0

        );

        return $result;
    }


    public function chooseMovie(int $id, int $userID)
    {

        switch ($_SESSION['favmovie']){
            case 0:
                $stmt = $this->database->connect()->prepare('
                UPDATE social SET fav_m1 = :id WHERE id = :userID
                ');
                $_SESSION['favmovie'] = $_SESSION['favmovie'] + 1;
                break;
            case 1:
                $stmt = $this->database->connect()->prepare('
                UPDATE social SET fav_m2 = :id WHERE id = :userID
                ');
                $_SESSION['favmovie'] = $_SESSION['favmovie'] + 1;
                break;
            case 2:
                $stmt = $this->database->connect()->prepare('
                UPDATE social SET fav_m3 = :id WHERE id = :userID
                ');
                $_SESSION['favmovie'] = 0;
                break;
        }
        $stmt->bindParam(':id', $id , PDO::PARAM_INT);
        $stmt->bindParam(':userID', $userID , PDO::PARAM_INT);
        $stmt->execute();
    }
}