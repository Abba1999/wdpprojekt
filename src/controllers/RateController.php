<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Rate.php';
require_once __DIR__.'/../repository/RateRepository.php';

class RateController extends AppController
{
    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/img/uploads/';

    private $messages = [];
    private $rateRepository;


    public function __construct()
    {
        parent::__construct();
        $this->rateRepository = new RateRepository();
    }

    public function rates(){
        $rates = $this->rateRepository->getRates();
        $this->render('rates',['rates' => $rates]);
    }

    public function addRate(){

        if($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])){

            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['file']['name'],
            );


            $rate = new Rate($_POST['title'],$_POST['description'],$_POST['stars'] ?: 0,$_FILES['file']['name'],0,0,null,$_SESSION['id']);
            $this->rateRepository->addRate($rate);

            return $this->render('rates', [
                'rates' => $this->rateRepository->getRates(),
                'messages' => $this->messages, 'rate' => $rate]);
        }

        $this->render("addRate",['messages' => $this->messages]);
    }

    public function search()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if ($contentType === "application/json") {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->rateRepository->getRateByTitle($decoded['search']));
        }
    }

    public function like(int $id){
        $this->rateRepository->like($id);
        http_response_code(200);
    }

    public function dislike(int $id){
        $this->rateRepository->dislike($id);
        http_response_code(200);
    }

    private function validate(array $file) : bool
    {
        if($file['size'] > self::MAX_FILE_SIZE){
            $this->messages[] = 'File is too large for destination file system.';
            return false;
        }
        if(!isset($file['type']) && !in_array($file['type'], self::SUPPORTED_TYPES )){
            $this->messages[] = 'File type is not supported';
            return false;
        }
        return true;
    }



}