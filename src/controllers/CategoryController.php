<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Movie.php';
require_once __DIR__.'/../repository/CategoryRepository.php';

class CategoryController extends AppController
{
    private $categoryRepository;


    public function __construct()
    {
        parent::__construct();
        $this->categoryRepository = new CategoryRepository();
    }

    public function MoviesCategories(){
        $geneVal = array();
        $ageVal = $_POST['age'];
        if(!empty($_POST['check'])) {
            foreach($_POST['check'] as $value){
                $geneVal[] = $value;
            }
        } else {
            $geneVal = 0;
        }




        $yearVal = $_POST['year'];
        $continentVal = $_POST['continent'];
//        echo $ageVal;
//        echo $geneVal[0];
//        echo $_POST['year'];
//        echo $_POST['continent'];
        $movies = $this->categoryRepository->getMoviesCategories($ageVal, $geneVal, $yearVal, $continentVal);
        $this->render('movies',['movies' => $movies]);
    }


    //getMovie();
}