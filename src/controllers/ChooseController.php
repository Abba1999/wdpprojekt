<?php
session_start();
require_once 'AppController.php';
require_once __DIR__.'/../models/Movie.php';
require_once __DIR__.'/../repository/ChooseRepository.php';

class ChooseController extends AppController
{
    private $chooseRepository;


    public function __construct()
    {
        parent::__construct();
        $this->chooseRepository = new ChooseRepository();
    }

    public function chooses(){

        $chooses = $this->chooseRepository->blankMovie();
        $this->render('chooses',['chooses' => $chooses]);
    }

    public function watch(){
        $geneVal = $_POST['cat-dd'];
        $friendVal = $_POST['friend'];


        $chooses = $this->chooseRepository->getMoviesChoose($geneVal);
        $this->render('chooses',['chooses' => $chooses]);
    }

    public function chooseMovie(int $id){
        $this->chooseRepository->chooseMovie($id, $_SESSION['ID']);
        http_response_code(200);
    }


    //getMovie();
}