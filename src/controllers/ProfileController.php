<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Profile.php';
require_once __DIR__.'/../repository/ProfileRepository.php';

class ProfileController extends AppController
{
    private $messages = [];
    private $profileRepository;


    public function __construct()
    {
        parent::__construct();
        $this->profileRepository = new ProfileRepository();
    }

    public function profile(){
        $profile = $this->profileRepository->getProfile();
        $this->render('profile',['profile' => $profile]);
    }

}