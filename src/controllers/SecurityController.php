<?php

session_start();

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

ini_set('memory_limit', '512MB');

class SecurityController extends AppController
{
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function login (){


        $email = $_POST["email"];
        $user = $this->userRepository->getUser($email);
        if(!$user){
            return $this->render('login', ['messages'=>['User not exist!']]);
        }


        $salt = $user->getSalt();
        if(!isset($_COOKIE['password'])){
            $password = sha1(md5($_POST["password"].$salt));
        }else{
            $password = sha1($_COOKIE['password']);
        }





        if($user->getEmail() !== $email){
            return $this->render('login', ['messages'=>['User with this email not exist!']]);
        }
        if($user->getPassword() !== $password){
            return $this->render('login', ['messages'=>['Wrong password!']]);
        }


        $_SESSION['ID'] = $user->getId();
        $_SESSION['role'] = $user->getRole();
        $_SESSION['favmovie'] = 0;
        $this->userRepository->enableUpdate($_SESSION['ID'],1);
        if(!empty($_POST["remember"])) {
            setcookie ("email",$_POST["email"],time()+ 3600);
            setcookie ("password",md5($_POST["password"].$salt),time()+ 3600);
        } else {
            setcookie("email","",time()-3600);
            setcookie("password","",time()-3600);
        }

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/movies");
    }

    public function register()
    {
        if (!$this->isPost()) {
            return $this->render('register');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $nick = $_POST['nick'];
        $salt = md5(time().uniqid());


        if ($password !== $confirmedPassword) {
            return $this->render('register', ['messages' => ['Please provide proper password']]);
        }

        if($email == null || $password == null || $confirmedPassword == null || $name == null || $surname == null || $nick == null){
            return $this->render('register', ['messages' => ['Blank spot']]);
        }

        //TODO try to use better hash function
        $user = new User($email, sha1(md5($password.$salt)), $name, $surname, null, $salt, 'false');
        $user->setNick($nick);

        $this->userRepository->addUser($user);

        return $this->render('login', ['messages' => ['You\'ve been succesfully registrated!']]);
    }

    public function logout (){


        $id_users = $_SESSION['ID'];
        $this->userRepository->enableUpdate($id_users,0);
        session_unset();

        setcookie("email","",time()-3600);
        setcookie("password","",time()-3600);


        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/");
    }
}