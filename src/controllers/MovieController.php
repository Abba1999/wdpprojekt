<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Movie.php';
require_once __DIR__.'/../repository/MovieRepository.php';

class MovieController extends AppController

{
    const MAX_FILE_SIZE = 1024 * 1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/img/uploads/';

    private $messages = [];
    private $movieRepository;


    public function __construct()
    {
        parent::__construct();
        $this->movieRepository = new MovieRepository();
    }

    public function movies()
    {
        $movies = $this->movieRepository->getMovies();
        $this->render('movies', ['movies' => $movies]);
    }


    public function search2()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if ($contentType === "application/json") {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->movieRepository->getMovieByTitle($decoded['search']));
        }
    }

    public function addMovie()
    {
        if (!$_SESSION['role'] == 2) {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/movies");
        }
        if ($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])) {

            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_FILES['file']['name'],
            );


            $movie = new Movie($_POST['title'], $_POST['viewerAge'], $_POST['year'],$_POST['continent']?: 0, $_FILES['file']['name'], $_POST['description'], null);
            $this->movieRepository->addMovie($movie);

            return $this->render('movies', [
                'movies' => $this->movieRepository->getMovies(),
                'messages' => $this->messages, 'movie' => $movie]);
        }

        $this->render("addMovie", ['messages' => $this->messages]);
    }
    private function validate(array $file) : bool
    {
        if($file['size'] > self::MAX_FILE_SIZE){
            $this->messages[] = 'File is too large for destination file system.';
            return false;
        }
        if(!isset($file['type']) && !in_array($file['type'], self::SUPPORTED_TYPES )){
            $this->messages[] = 'File type is not supported';
            return false;
        }
        return true;
    }

}

