<?php


class Movie
{
    private $title;
    private $viewer_age;
    private $create_year;
    private $continent;
    private $image;
    private $description;
    private $id;

    public function __construct(string $title, int $viewer_age, int $create_year, string $continent, string $image, string $description, $id=null)
    {
        $this->title = $title;
        $this->viewer_age = $viewer_age;
        $this->create_year = $create_year;
        $this->continent = $continent;
        $this->image = $image;
        $this->description = $description;
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getViewerAge(): int
    {
        return $this->viewer_age;
    }

    /**
     * @param int $viewer_age
     */
    public function setViewerAge(int $viewer_age): void
    {
        $this->viewer_age = $viewer_age;
    }

    /**
     * @return int
     */
    public function getCreateYear(): int
    {
        return $this->create_year;
    }

    /**
     * @param int $create_year
     */
    public function setCreateYear(int $create_year): void
    {
        $this->create_year = $create_year;
    }

    /**
     * @return string
     */
    public function getContinent(): string
    {
        return $this->continent;
    }

    /**
     * @param string $continent
     */
    public function setContinent(string $continent): void
    {
        $this->continent = $continent;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }



}