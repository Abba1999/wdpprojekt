<?php


class User
{
    private  $email;
    private $password;
    private $name;
    private $surname;
    private $nick;
    private $id;
    private $salt;
    private $role;

    public function __construct(string $email, string $password, string $name, string $surname, $id=null, $salt=null, $role='false')
    {
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
        $this->id = $id;
        $this->salt= $salt;
        $this->role = $role;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt($salt): void
    {
        $this->salt = $salt;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }


    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    public function getNick()
    {
        return $this->nick;
    }

    public function setNick(string $nick): void
    {
        $this->nick = $nick;
    }


}