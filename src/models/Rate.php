<?php


class Rate
{
    private $title;
    private $description;
    private $stars;
    private $image;
    private $likes;
    private $dislikes;
    private $id;
    private $userID;

    public function __construct(string $title, string $description, int $stars, string $image, $likes=0, $dislikes=0, $id=null, $userID=null)
    {
        $this->title = $title;
        $this->description = $description;
        $this->stars = $stars;
        $this->image = $image;
        $this->likes = $likes;
        $this->dislikes = $dislikes;
        $this->id = $id;
        $this->userID = $userID;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getStars(): int
    {
        return $this->stars;
    }

    public function setStars(int $stars): void
    {
        $this->stars = $stars;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    public function getLikes(): int
    {
        return $this->likes;
    }

    public function setLikes(int $likes): void
    {
        $this->likes = $likes;
    }

    public function getDislikes(): int
    {
        return $this->dislikes;
    }

    public function setDislikes(int $dislikes): void
    {
        $this->dislikes = $dislikes;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }





}