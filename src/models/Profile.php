<?php


class Profile
{
    private $id;
    private $id_users;
    private $fav_m1;
    private $fav_m2;
    private $fav_m3;
    private $fav_r1;
    private $fav_r2;
    private $fav_r3;
    private $fav_c1;
    private $fav_c2;
    private $fav_c3;
    private $nick;
    private $friend0;
    private $friend1;
    private $friend2;

    public function __construct($id = null, $id_users = null, $fav_m1, $fav_m2, $fav_m3, $fav_r1, $fav_r2, $fav_r3, $fav_c1, $fav_c2, $fav_c3, $nick,$friend0, $friend1='blank.png',$friend2='blank.png')
    {
        $this->id = $id;
        $this->id_users = $id_users;
        $this->fav_m1 = $fav_m1;
        $this->fav_m2 = $fav_m2;
        $this->fav_m3 = $fav_m3;
        $this->fav_r1 = $fav_r1;
        $this->fav_r2 = $fav_r2;
        $this->fav_r3 = $fav_r3;
        $this->fav_c1 = $fav_c1;
        $this->fav_c2 = $fav_c2;
        $this->fav_c3 = $fav_c3;
        $this->nick = $nick;
        $this->friend0 = $friend0;
        $this->friend1 = $friend1;
        $this->friend2 = $friend2;
    }

    public function getIdUsers()
    {
        return $this->id_users;
    }

    public function getFavM1()
    {
        return $this->fav_m1;
    }

    public function getFavM2()
    {
        return $this->fav_m2;
    }

    public function getFavM3()
    {
        return $this->fav_m3;
    }

    public function getFavR1()
    {
        return $this->fav_r1;
    }

    public function getFavR2()
    {
        return $this->fav_r2;
    }

    public function getFavR3()
    {
        return $this->fav_r3;
    }

    public function getFavC1()
    {
        return $this->fav_c1;
    }

    public function getFavC2()
    {
        return $this->fav_c2;
    }

    public function getFavC3()
    {
        return $this->fav_c3;
    }

    public function getNick()
    {
        return $this->nick;
    }

    public function getFriend0()
    {
        return $this->friend0;
    }


    public function getFriend1(): string
    {
        return $this->friend1;
    }

    public function getFriend2(): string
    {
        return $this->friend2;
    }


}